# Copyright 2019 James Davidson
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

# Set up the Ubuntu VM by changing:
	# hostname
	# uname
	# passwd

# Is user root?

# https://askubuntu.com/questions/15853/how-can-a-script-check-if-its-being-run-as-root/15856#15856
is_root() {
	if [[ $EUID -ne 0 ]]; then
		echo 'Script must be run as root!'
		echo '	* Change root user password:'
		echo '		* sudo passwd root'
		echo '	* Current user must log off'
		echo '		* exit'
		echo '	* Log back in as root'
   	exit 1
	fi
}

is_root

# Change the hostname

echo 'Enter new hostname:'
read new_host
# https://www.cyberciti.biz/faq/linux-change-hostname/
hostnamectl set-hostname "$new_host"
# Change hostname in /etc/hosts to avoid sudo errors
old_host='osboxes'
sed -i s/$old_host/$new_host/ /etc/hosts

# Change username

old_uname='osboxes'
new_uname='administrator'
# https://askubuntu.com/questions/34074/how-do-i-change-my-username/317008#317008
usermod -l $new_uname $old_uname -d /home/$new_uname -m

# Change passwd

new_passwd='letmein'
# https://www.systutorials.com/39549/changing-linux-users-password-in-one-command-line/
# BUG passwd does not respect quiet flag
echo -e "$new_passwd\n$new_passwd" | passwd -q administrator
