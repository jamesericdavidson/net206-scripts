# Copyright 2019 James Davidson
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

# RUN FROM THE CONTROL NODE

# Ping Node01, Node02, Node03 and append the output to ping_results.txt

ping_node() {
	# Ping the node ($2) X times ($1), appending the results to an output file ($3)
	ping -c $1 $2 >> $3
}

count=4
ping_output='ping_results.txt'
node01='node01.cluster.com'
node02='node02.cluster.com'
node03='node03.cluster.com'

ping_node $count $node01 $ping_output
ping_node $count $node02 $ping_output
ping_node $count $node03 $ping_output

# Create new directory on all nodes, creating a file containing the node's hostname

user='administrator'
directory='temp_dirname'
file='host.txt'

create_dir_on_node() {
	# NOTE: ssh keypairs have already been created and copied to nodes

	# ssh into the remote server ($2) as given user ($1)
	ssh $1@$2 -t "mkdir $HOME/$directory; cat /etc/hostname > $HOME/$directory/$file"

}

create_dir_on_node $user $node01
create_dir_on_node $user $node02
create_dir_on_node $user $node03

# Verify file contents on node

hostname_output='file_contents.txt'

verify_results_on_node() {
	# Print the server's (fake) domain name
	echo $2; echo
	# List the contents of $directory, then print the contents of $file
	ssh $1@$2 -t "ls * $HOME/$directory; echo; cat $HOME/$directory/$file"
	echo; echo '--------------------'; echo
}

verify_results_on_node $user $node01 >> $hostname_output
verify_results_on_node $user $node02 >> $hostname_output
verify_results_on_node $user $node03 >> $hostname_output
