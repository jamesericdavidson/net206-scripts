# Copyright 2019 James Davidson
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

# RUN FROM THE CONTROL NODE

# Clean up test files

rm_dir(){
	ssh $1@$2 -t "rm -r /home/administrator/temp_dirname"
}

node01=192.168.56.102
node02=192.168.56.103
node03=192.168.56.104
user='administrator'

rm_dir $user $node01
rm_dir $user $node02
rm_dir $user $node03

# Clean up local files

rm ~/NET206/*.txt
