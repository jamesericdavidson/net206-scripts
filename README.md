# NET206 Scripts

Scripts for NET206 Coursework - Servers, Datacentres and Cloud

*Administer Ubuntu nodes using Bash scripts*

---

The scripts encompass two tasks:

1. Ping all of the nodes, and write the output to a file
2. Use SSH to create a directory on each node, and a file within it containing the node's hostname

```
Copyright 2019 James Davidson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
